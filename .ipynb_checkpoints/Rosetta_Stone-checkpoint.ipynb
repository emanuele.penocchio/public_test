{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fmoc-Cl as a Rosetta Stone between Stochastic Thermo, Astumian and dissipative self-assembly (Ragazzon/Prins)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A quick notebook for internal use in which I sketch why those three approaches are fully compatible, and to highlight why -- in my opinion -- our analysis fully contains the others, thus showing how the principle of kinetic asymmetry can be derived as a consequence of the second principle of thermodynamics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Condition for a positive $J_\\mathrm{ss}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we take our decomposition of the entropy production:\n",
    "\n",
    "$ \n",
    "\\dot{\\Sigma} = \\overbrace{\\underbrace{J_\\mathrm{ss} R\\log \\frac{k_{+\\Delta}k'_{+\\Delta}}{k_{-\\Delta}k'_{-\\Delta}}}_{\\dot{S}^\\mathrm{mh}_\\mathrm{r}} + \\underbrace{J_\\mathrm{ss} R\\log \\frac{[\\mathrm{1^D_H}][\\mathrm{1^H_D}]}{[\\mathrm{1^D_D}][\\mathrm{1^H_H}]}}_{\\dot{I}}}^{\\dot{\\Sigma}^\\mathrm{mh}} + \\overbrace{\\underbrace{\\mathrm{I_F}\\frac{(\\mu_\\mathrm{F} - \\mu_\\mathrm{W})}{T} + J_\\mathrm{ss}R\\log \\frac{k_{-\\Delta}k'_{-\\Delta}}{k_{+\\Delta}k'_{+\\Delta}}}_{\\dot{S}^\\mathrm{ch}_\\mathrm{r}} - \\dot{I}}^{\\dot{\\Sigma}^\\mathrm{ch}}\n",
    "$\n",
    "\n",
    "and we focus on the entropy production of the mechanical part, the following relation holds\n",
    "\n",
    "$\\dot{\\Sigma}^\\mathrm{mh} = J_\\mathrm{ss}R \\left(\\log \\frac{k_{+\\Delta}k'_{+\\Delta}}{k_{-\\Delta}k'_{-\\Delta}} + \\log \\frac{[\\mathrm{1^D_H}][\\mathrm{1^H_D}]}{[\\mathrm{1^D_D}][\\mathrm{1^H_H}]}\\right) \\ge 0$\n",
    "\n",
    "This implies that in order to have a positive $J_\\mathrm{ss}$ the quantity in parenthesis has to be positive too.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connection to Ragazzon and Prins' $K_r$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For any set of parameters, the expressions of the stationary state consentrations can be obtained analytically and plugged into the inequality above. It can be proved \\*(see below for a sketch of the proof) that\n",
    "\n",
    "$\\log \\left( \\frac{k_{+\\Delta}k'_{+\\Delta}}{k_{-\\Delta}k'_{-\\Delta}} \\frac{[\\mathrm{1^D_H}][\\mathrm{1^H_D}]}{[\\mathrm{1^D_D}][\\mathrm{1^H_H}]} \\right) \\ge 0 $\n",
    "\n",
    "implies\n",
    "\n",
    "$\\frac{k_{+\\Delta}k'_{+\\Delta} (k^\\mathrm{h}_{-\\mathrm{F}} + k^\\mathrm{h}_{-\\mathrm{W}})^2([\\mathrm{F}]k_{+\\mathrm{F}} + [\\mathrm{W}]k_{+\\mathrm{W}})^2}{k_{-\\Delta}k'_{-\\Delta}([\\mathrm{F}]k^\\mathrm{h}_{+\\mathrm{F}} + [\\mathrm{W}]k^\\mathrm{h}_{+\\mathrm{W}})^2 (k_{-\\mathrm{F}} + k_{-\\mathrm{W}})^2} = K_r \\ge 1$\n",
    "\n",
    "which is the \"ratcheting constant\" witten for the Fmoc-Cl rotatory motor. The proof is just based on algebra, which is cumbersome for the general case, but straightforward for the experimental case. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connection to Astumian's kinetic asymmetry"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As also shown in Ragazzon and Prins' perspective, their $K_r$-rule is equivalent to the kinetic asymmetry rule. This is proven in few passages:\n",
    "\n",
    "$K_r = \\frac{k_{+\\Delta}k'_{+\\Delta} (k^\\mathrm{h}_{-\\mathrm{F}})^2 ([\\mathrm{F}]k_{+\\mathrm{F}})^2 }{k_{-\\Delta}k'_{-\\Delta}([\\mathrm{F}]k^\\mathrm{h}_{+\\mathrm{F}})^2(k_{-\\mathrm{F}})^2} \\frac{ ( 1 + \\frac{k^\\mathrm{h}_{-\\mathrm{W}}}{k^\\mathrm{h}_{-\\mathrm{F}}})^2(1 + \\frac{[\\mathrm{W}]k_{+\\mathrm{W}}}{[\\mathrm{F}]k_{+\\mathrm{F}}})^2}{(1 + \\frac{[\\mathrm{W}]k^\\mathrm{h}_{+\\mathrm{W}}}{[\\mathrm{F}]k^\\mathrm{h}_{+\\mathrm{F}}})^2 (1 + \\frac{k_{-\\mathrm{W}}}{k_{-\\mathrm{F}}})^2} = \\frac{ ( 1 + \\frac{k^\\mathrm{h}_{-\\mathrm{W}}}{k^\\mathrm{h}_{-\\mathrm{F}}})^2(1 + \\frac{[\\mathrm{W}]k_{+\\mathrm{W}}}{[\\mathrm{F}]k_{+\\mathrm{F}}})^2}{(1 + \\frac{[\\mathrm{W}]k^\\mathrm{h}_{+\\mathrm{W}}}{[\\mathrm{F}]k^\\mathrm{h}_{+\\mathrm{F}}})^2 (1 + \\frac{k_{-\\mathrm{W}}}{k_{-\\mathrm{F}}})^2}$\n",
    "\n",
    "where the Wegscheider's conditions were applied in the last passage. The final expression is the \"analog\" of eq. (7) in Astumian's _Nat. Commun. 10, 3837 (2019)_, but there he is using a 4-state model different from ours.\n",
    "\n",
    "(just let me comment -- among us -- that not only Astumian's claim that \"the “local detailed balance” hypothesis is incorrect\" is incorrect itself, but his own equation is a consequence of ldb and equilibrium thermodynamics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Importantly, $K_r$ is a polynomial in $[F]$, and therfore the equation $K_r -1 = 0$ may have many solutions on varying of $[F]$ while keepeng all the other parameters constants. I think that the thermodynamic picture is the clearest one in distingushing among those possibilities:\n",
    "\n",
    "- One solution always exists and represents the equlibrium condition: no matter how asymmetric my system is, if $F$ is such that $\\mu_\\mathrm{F} - \\mu_\\mathrm{W} = 0$ then no directional current may arise. If no work is done on the system, it can't be driven out-of-eq\n",
    "- All the other solutions, if they exist, represent cases in wich the system is \"stalled\" in a thermodynamic languace, that is a condition in which energy- and information- flows compensate for each other, thus stopping the current\n",
    "- It may happen that $k_r = 1 \\, \\forall \\, [\\mathrm{F}]$. This means that the two set of states are completly decoupled and no free-energy flows are possible "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## \\* Proof of the implication $\\Sigma^\\mathrm{mh} > 0 \\rightarrow K_r > 1$ (technical)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To prove it with pencil and paper, I rely on some results from graph theory and techniques similar to those used by Hill.\n",
    "\n",
    "The starting point is the following analytcal expression of the steady state concentrations of each species in the network:\n",
    "\n",
    "$[x_\\alpha] = \\frac{L_M}{\\mathcal{N}} \\sum_{t \\in \\mathcal{T}_\\alpha} \\prod_{\\rho \\in t} \\tilde{k}_\\rho$\n",
    "\n",
    "Where $L_M$ is the conserved total concentration, $\\mathcal{N} = \\sum_\\alpha \\sum_{t \\in \\mathcal{T}_\\alpha} \\prod_{\\rho \\in t} k_\\rho $ is a normalizing denominator, $\\mathcal{T}_\\alpha$ is the set of spanning trees rooted in vertex $\\alpha$, and $\\tilde{k}_\\rho$ is the pseudo-first-order rate contant of reaction $\\rho$. A rooted spanning tree is a spanning tree with its edges oriented such that all edges point towards the root.\n",
    "\n",
    "By using such result, the condition\n",
    "\n",
    "$\\log \\left( \\frac{k_{+\\Delta}k'_{+\\Delta}}{k_{-\\Delta}k'_{-\\Delta}} \\frac{[\\mathrm{1^D_H}][\\mathrm{1^H_D}]}{[\\mathrm{1^D_D}][\\mathrm{1^H_H}]} \\right) \\ge 0 $\n",
    "\n",
    "can be rewrited as\n",
    "\n",
    "$\n",
    "\\left( k_{+\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_H}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho \\right) \\cdot\n",
    "\\left( k'_{+\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_D}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho \\right) -\n",
    "\\left( k_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_D}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho \\right) \\cdot \n",
    "\\left( k'_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_H}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho \\right) > 0\n",
    "$.\n",
    "\n",
    "By defining $\\mathcal{P}_1 = \\prod_{\\rho \\in \\mathcal{T}_\\mathrm{cw}} \\tilde{k}_\\rho$ and $\\mathcal{P}_3 = \\prod_{\\rho \\in \\mathcal{T}_\\mathrm{ccw}} \\tilde{k}_\\rho$ as the product of all forward clockwise (resp. counterclockwise) pseudo reaction constants, we can manipulate the above expression as follows\n",
    "\n",
    "$\n",
    "\\left( k_{+\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_H} \\land k_{-\\Delta} \\in t} \\prod_{\\rho \\in t} \\tilde{k}_\\rho + \\mathcal{P}_1 \\right) \\cdot\n",
    "\\left( k'_{+\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_D} \\land k'_{-\\Delta} \\in t} \\prod_{\\rho \\in t} \\tilde{k}_\\rho + \\mathcal{P}_1 \\right) -\n",
    "\\left( k_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_D} \\land k_{+\\Delta} \\in t} \\prod_{\\rho \\in t} \\tilde{k}_\\rho +\\mathcal{P}_3 \\right) \\cdot \n",
    "\\left( k'_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_H} \\land k'_{+\\Delta} \\in t} \\prod_{\\rho \\in t} \\tilde{k}_\\rho + \\mathcal{P}_3 \\right) =\n",
    "0 + \n",
    "(\\mathcal{P}_1^2 - \\mathcal{P}_3^2) + \n",
    "\\mathcal{P}_1 \\underbrace{\\left[ \n",
    "k_{+\\Delta} k_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_H} \\land k_{-\\Delta} \\in t} \\frac{1}{k_{-\\Delta}}\\prod_{\\rho \\in t} \\tilde{k}_\\rho +  \n",
    "k'_{+\\Delta}k'_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_D} \\land k'_{-\\Delta} \\in t} \\frac{1}{k'_{-\\Delta}}\\prod_{\\rho \\in t} \\tilde{k}_\\rho\n",
    "\\right]}_{\\Gamma} -\n",
    "\\mathcal{P}_3 \\underbrace{\\left[ \n",
    "k_{+\\Delta} k_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^D_D} \\land k_{+\\Delta} \\in t}\\frac{1}{k_{+\\Delta}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho +  \n",
    "k'_{+\\Delta}k'_{-\\Delta} \\sum_{t \\in \\mathcal{T}_\\mathrm{1^H_H} \\land k'_{+\\Delta} \\in t}\\frac{1}{k'_{+\\Delta}} \\prod_{\\rho \\in t} \\tilde{k}_\\rho\n",
    "\\right]}_{\\Gamma} =\n",
    "(\\mathcal{P}_1 - \\mathcal{P}_3) \\cdot \\left( \\mathcal{P}_1 + \\mathcal{P}_3 + \\Gamma \\right)\n",
    "$.\n",
    "\n",
    "Which allows us to conclude that the condition is satisfied iff $(\\mathcal{P}_1 - \\mathcal{P}_3) > 0$\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some more considerations on the connection with $K_r$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By looking again at the entropy production due to mechanical transitions and by introducing the chemical affinity as the quantity $\\mathcal{A}_\\rho = - \\Delta_\\rho G$, we can rewrite it as\n",
    "\n",
    "$\\dot{\\Sigma}^\\mathrm{mh} = J_\\mathrm{ss}R \\left(\\log \\frac{k_{+\\Delta}k'_{+\\Delta}}{k_{-\\Delta}k'_{-\\Delta}} + \\log \\frac{[\\mathrm{1^D_H}][\\mathrm{1^H_D}]}{[\\mathrm{1^D_D}][\\mathrm{1^H_H}]}\\right) = \\frac{J_\\mathrm{ss}}{T} \\left( \\mathrm{A}_\\Delta +\\mathrm{A}_{\\Delta'} \\right) \\ge 0$\n",
    "\n",
    "which is insightful because it tells us that the quantity in parenthesis is the free-energy stored in the mechanical degrees of freedom when the system is in its stationary state. This connects to -- and extends -- the concept of $K_r$, which Ragazzon and Prins interpret energetically in the following way:\n",
    "\n",
    "they write that \"in the limit of rapid equilibration of steps 1 and 3 (not the case for Fmoc-Cl motor, Ed.) -RTlnKr can be correctly approximated to the energy stored in the self-assembling steps.\"\n",
    "\n",
    "So, we not only can prove that the condition $(\\mathrm{A}_\\Delta +\\mathrm{A}_{\\Delta'}) > 0$ implies $K_r > 1$, but we clarify the role played by the energy stored in the mechanical steps in any regime and not only when the chemical steps are super fast wrt the mechanical ones (where one would have $\\mathrm{A}_\\Delta +\\mathrm{A}_{\\Delta'} \\approx RT \\log (K_r)$ as Ragazzon and Prins correctly stated).\n",
    "\n",
    "Morever since the following relation always holds\n",
    "\n",
    "$J_\\mathrm{ss} = k_\\Delta [\\mathrm{1^D_H}] \\left( 1 - e^{-\\mathrm{A}_\\Delta / RT} \\right)$\n",
    "\n",
    "and since in all scenarios but scenario 1 we have by symmetry that $\\mathrm{A}_\\Delta = \\mathrm{A}_{\\Delta'}$, we learn that, as a rule of thumb, increasing energy and information flows is *usually* a good thing for the sake of increasing the current -- a confirmation via simulation is always needed to be sure!\n",
    "\n",
    "**Now it comes one of the most interesting conclusions according to me**. Sometimes, increasing energy and information flows also means to increase $K_r$ and kinetic asymmetry of the system, but some others no (for example in scenario 1 and scenario 2 - \"Astumian friendly\"). This shows that, given a certain $K_r$, there is still room for improving the system's performance by acting for example on the stability of the states. This is particularly important, because it seems to me that Astumian sometimes claims that you cannot change the current without changing the kinetic asymmetry. Actually, things are more subtle: $K_r$ and kinetic asymmetry tells us whether the system can cycle or not, and in which direction, but the actual magnitute of the current is given by all the other parameters, stability of mechanical states included. Thermodynamic picture, chemical intuition and simulations helps a lot in finding out the optimal conditions, and I think that showing how put this in practice may be a powerful message! \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
