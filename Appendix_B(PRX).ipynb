{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A very quick notebook to explain Appendix B of _Phys. Rev. X_ **6**, 041064 (2016)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equations B1 and B2\n",
    "\n",
    "To start, we need to understand why the stochastic entropy (the entropy of a system with very few molecules) in equation B1 reads\n",
    "\n",
    "$S(\\mathbf{n}) = \\underbrace{-k_\\mathrm{B} \\, ln \\, p_t (\\mathbf{n})}_{S_\\mathrm{h}} + \\underbrace{s(\\mathbf{n})}_{S_\\mathrm{c}}$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Shannon entropy\n",
    "\n",
    "As written in the PRX paper, the first term ($S_\\mathrm{h}$) is the so-called [Shannon entropy contribution](https://en.wikipedia.org/wiki/Entropy_(information_theory)), which is an entropic contribution of informational nature which is always present in stochastic systems. You can think of it as the information content of that particular state of the system. \n",
    "\n",
    "For instance, suppose that our system is an unbiased coin with probability 1/2 to show the head-face after a toss and vice-versa for the tail-face. Its Shannon entropy in each of the two possible states will be\n",
    "\n",
    "$S_\\mathrm{h} (H) = S_\\mathrm{h} (T) = k_\\mathrm{B} \\, \\ln 2$\n",
    "\n",
    "which is the information stored in one bit (according to Landauer's bound, it is also the minimum amount of free-energy you need to erase/reset one bit of information). If a certain state has probabilty 1 (no other states are possible for that system) than the information content of that state is null. \n",
    "\n",
    "For CRN with very few molecules, we do have a similar contribution due to the fact that reactions events are stochastic, like coin tossings. Suppose to have a system with only the reaction $\\mathrm{A} \\rightleftarrows \\mathrm{B}$ with equal forward and backward kinetic constants. This is exactly analogous to a coin: you just now that the molecule has the same probability to be in state (conformer) A or B."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boltzmann entropy\n",
    "\n",
    "Suppose to have 2 molecules instead of one in your system. You then have the 4 possible states AA, AB, BA, BB with probability 1/4 each and Shannon entropy $k_\\mathrm{B} \\, \\ln 4$ (information about two independent bits!).\n",
    "\n",
    "Suppose now that the molecules are not distinguishable, that is: you don't measure -- if you ever can! -- something like \"the first molecule is in state A and the second in state B\" but rather \"there is one molecule of A and one of B\". In the second scenario, the states of the system (possible outcomes of a measure) are just 3: (A , B) = (2, 0); (1,1); (0,2), with probabilities 1/4;1/2;1/4. In other words, we have a degeneracy here for the \"mixed\" state, which brings a _configurational_ entropy in the form of what in statistichal mechanics is called \"Boltzmann entropy\" $S_\\mathrm{B} = k_\\mathrm{B} \\ln W (\\mathbf{n})$, where W is the multiplicity of state n (how many different microscopic configurations are there which gives state n). In the example, the multiplicity of the three states reads:\n",
    "\n",
    "$W (2,0) = \\frac{(n_A + n_B)!}{n_A! n_B!} = \\frac{2!}{2! 0!} = 1$\n",
    "\n",
    "$W (1,1) = \\frac{(n_A + n_B)!}{n_A! n_B!} = \\frac{2!}{1! 1!} = 2$\n",
    "\n",
    "$W (0,2) = \\frac{(n_A + n_B)!}{n_A! n_B!} = \\frac{2!}{0! 2!} = 1$\n",
    "\n",
    "which tell us that for the state (1,1) we also have a Boltzmann entropy $S_\\mathrm{B} = k_\\mathrm{B} \\ln 2$, which is null for the other two states.\n",
    "\n",
    "Crucially, note that all the three states have the same stochastic entropy $S = S_\\mathrm{h} + S_\\mathrm{B} = k_\\mathrm{B} \\, \\ln 4$. Indeed, the physical entropy in the system is the same independently from our ability to measure the system: the entropy we \"loose\" because of the fact that the molecule are indistinguishable, we \"gain\" because of multiplicity. This means that the entropy of the state (1,1) is the same whether we can distinguish or not the two molecules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Internal entropy\n",
    "\n",
    "Finally, A and B may have a different entropy of formation depending on their chemical nature. For example, one of the two conformers may have less rotational freedom or different degeneracies in its vibrational states _etc_. We take into account all this microscopic details with an internal entropy term which only depends on the \"chemical identity\" of the molecule and which is additive: $S_\\mathrm{i} (\\mathbf{n}) = \\sum_\\sigma n_\\sigma s^\\circ_\\sigma$. To compute this term, you can use partition functions of the molecules from statistichal mechanics. Thanks to this term, states (2,0) and (0,2) may have different entropies."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configurational entropy (for an ideal solution)\n",
    "\n",
    "In the PRX paper, they have call \"configurational entropy\" the quantity $S_\\mathrm{c} = S_\\mathrm{i} + S_\\mathrm{B}$.\n",
    "\n",
    "Now let's try to write the Boltzmann term of a \"solution\", that is a system with discrete number of molecules of different species, one of which (the solvent S) is way more aboundant ($n_S >> \\sum_\\sigma n_\\sigma$):\n",
    "\n",
    "$W = \\frac{(n_S + \\sum_\\sigma n_\\sigma)!}{n_S! \\prod_\\sigma n_\\sigma!}$\n",
    "\n",
    "using the stirling approximation: $a! \\approx \\sqrt{2\\pi a} (a/e)^a$:\n",
    "\n",
    "$\n",
    "\\frac{(n_S + \\sum_\\sigma n_\\sigma)!}{n_S!} = \\sqrt{1 + \\frac{\\sum_\\sigma n_\\sigma}{n_S}} \\left( e^{-\\sum_\\sigma n_\\sigma} (1 + \\frac{\\sum_\\sigma n_\\sigma}{n_S})^{n_S + \\sum_\\sigma n_\\sigma} \\right) n_S^{\\sum_\\sigma n_\\sigma} = \\left( 1 + \\frac{\\sum_\\sigma n_\\sigma}{n_S} \\right)^{1/2 + \\sum_\\sigma n_\\sigma } \\left( e^{-\\sum_\\sigma n_\\sigma} (1 + \\frac{\\sum_\\sigma n_\\sigma}{n_S})^{n_S} \\right) n_S^{\\sum_\\sigma n_\\sigma}\n",
    "$\n",
    "\n",
    "Note that, by using $\\frac{\\sum_\\sigma n_\\sigma}{n_S} << 1$ both the parenthesis in the last expression go to 1 (rembering that $1 + x \\approx e^x$ when $x$ is small). We are then left with:\n",
    "\n",
    "$W = \\frac{n_S^{\\sum_\\sigma n_\\sigma}}{\\prod_\\sigma n_\\sigma!} = \\prod_\\sigma \\frac{n_S^{n_\\sigma}}{n_\\sigma!}$\n",
    "\n",
    "which is the Boltzmann term used in equation B2 of the PRX paper."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equation B3\n",
    "\n",
    "With equation B1 and B2 we can compute the entropy of a solution with few molecules of solutes, which is a stochastic system. As the size of the system increases (large particle limit where $n_\\sigma >> 1$ too) the system becomes deterministic. This could be rigorously justified by using the central limit theorem, but for the sake of understanding the appendix, just think to the probability of observing a certain state after tossing a huge amount of coinis: the more the coins, the less significant will be the probapility to observe a non 50/50 distribution. In other words, in the large particle limit we have a certain state (the most probable state) with probability ~1 to be observed. This means that the Shannon entropy of a deterministic system (the average entropy weighted over all possible states) is zero, and we are left with just the configurational entropy:\n",
    "\n",
    "$\\langle S \\rangle = S (\\mathbf(\\hat{n}) = \\sum_\\sigma \\left( \\hat{n}_\\sigma s^\\circ_\\sigma - k_\\mathrm{B} \\ln \\frac{n_\\sigma!}{n_S^{n_\\sigma}}\\right)$\n",
    "\n",
    "Crucially, $n_\\sigma$ is now a big number too, and we can thus apply again the Stirling's approximation (now in the more familiar form $\\ln a! = a \\ln a - a$) and obtain equation B4. The last part of the appendix should be quite understandable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
